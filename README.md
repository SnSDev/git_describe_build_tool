# git_describe_build_tool

Retrieve Git tag, store as environment variable at build time

The command `git describe --tags`

> ... finds the most recent tag that is reachable from a commit. If
> the tag points to the commit, then only the tag is shown. Otherwise,
> it suffixes the tag name with the number of additional commits on
> top of the tagged object and the abbreviated object name of the most
> recent commit. The result is a "human-readable" object name which
> can also be used to identify the commit to other git commands ...
>
> -- Git Manual

Example
-------

To use, simply add as a build dependency to your `cargo.toml`

```
[package]
build = "build.rs"

[build-dependencies]
git_describe_build_tool = "~1.0.0"
```

Then, add `build.rs`

```rust
use git_describe_build_tool::prelude::*;

fn main() {
    git_build_script();
}
```

Then, in your code, whenever you want to reference the current version
use the `env!` macro

```rust
pub const GIT_COMMIT_DESCRIBE: &str = env!("GIT_COMMIT_DESCRIBE");

assert_eq! {
    "0.11.0-51-g4446464",
    GIT_COMMIT_DESCRIBE
}
```

License: MIT
